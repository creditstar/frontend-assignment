# Front-end Developer Test Assignment

The objective of this assignment is to familiarise with the style, quality and level of the code produced by the front-end developer.

We assume that interested applicants would be able to complete the test within few days.

Once the test is completed please forward the results to <it.career@creditstar.com>.

## Assignment

Create a simple single page application with fixed-date loan repayment calculator.

As an example you could use existing calculator at https://www.creditstar.co.uk/ homepage.

**Calculator must have these input fields:**

1. Loan amount (eg. 1000.00 EUR)

2. Loan period (eg. 5 months)

3. Loan repayment date (eg. every 31st day of the month)

**Calculator should display:**

1. Number of instalments

2. Date of each instalment

4. Amount of each instalment

4. Interest part of each instalment

Assume that the interest rate is fixed: 25% (per annum)

## Requirements
* Use any available front-end libraries or frameworks of your choice.
* Provide a link to a Github or Bitbucket code repository.
* Provide application installation/deployment instructions.
* Application should be responsive and mobile-friendly.
* Finished code should be production ready.

## Bonus
* Add some unit testing code coverage for the main calculator functionality.

## Evaluation Criteria

* Code functionality, documentation, formatting, readability and quality.
* Usage of available tools, libraries and frameworks.
* Git usage. How commits are created and commented. We want to see the process of the work.


Should any technical questions arise feel free to contact: <it.career@creditstar.com>